# clean-envs-bun

Delete conda and Pipenv virtual environments via Bun Shell.

## Development

```bash
curl -fsSL https://bun.sh/install | bash -s "bun-v1.0.25"
```

```bash
bun install
```

```bash
bun run dev:conda
```

```bash
bun run dev:pipenv
```
