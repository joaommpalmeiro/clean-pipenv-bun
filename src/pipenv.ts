import { $ } from "bun";
import { access, constants } from "node:fs/promises";
import { homedir } from "node:os";
import { resolve } from "node:path";

for await (const venv of $`ls $HOME/.local/share/virtualenvs`.lines()) {
    // Workaround because the last `venv` value is an empty string:
    if (venv) {
        const venvPath = resolve(homedir(), ".local/share/virtualenvs", venv);
        const projectPath = resolve(venvPath, ".project");

        const project = await $`cat ${projectPath}`.text();

        try {
            await access(project, constants.F_OK);
            await $`cd ${project} && pipenv --rm`;
            console.log(`${venvPath} for ${project} ✓`);
        } catch {
            // If the project folder no longer exists:
            await $`rm -rf ${venvPath}`;
            console.log(`${venvPath} ✓`);
        }
    }
}

console.log("Done!");
