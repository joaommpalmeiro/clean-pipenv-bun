import { $ } from "bun";

const base = await $`conda info --base`.text();
// console.log(JSON.stringify(base));

const { envs } = await $`conda env list --json`.json();

for (const env of envs) {
    if (env !== base.trim()) {
        await $`conda env remove -p ${env}`;
        console.log(`${env} ✓`);
    }
}

console.log("Done!");
