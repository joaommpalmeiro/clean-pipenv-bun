# Notes

- https://github.com/gtalarico/pipenv-pipes
- https://pipenv.pypa.io/en/latest/virtualenv.html
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/project.py#L421
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/project.py#L534: `_get_virtualenv_hash()`
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/utils/virtualenv.py#L200: `cleanup_virtualenv()`
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/cli/command.py#L57 + https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/cli/command.py#L153
- https://docs.python.org/3/library/hashlib.html#hashlib.sha256
- https://bun.sh/docs/runtime/shell
- https://bun.sh/docs/installation + https://bun.sh/docs/quickstart
- https://bun.sh/blog/the-bun-shell
- https://ashari.me/posts/bun-making-shell-scripts-simple-in-javascript
- https://news.ycombinator.com/item?id=39071688
- https://nodejs.org/en/about/previous-releases
- https://bun.sh/docs/typescript
- https://bun.sh/docs/cli/run
- https://bun.sh/docs/cli/bunx: "(...) `bunx` (...) Bun's equivalent of `npx` (...)"
- https://bun.sh/docs/install/registries#npmrc
- https://bun.sh/docs/runtime/bunfig
- https://bun.sh/docs/installation#uninstall
- https://github.com/oven-sh/bun/releases
- https://www.npmjs.com/package/bun-types
- https://bun.sh/docs/cli/install
- https://bun.sh/docs/runtime/bunfig#telemetry
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/patched/pip/_internal/utils/appdirs.py
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/patched/pip/_internal/configuration.py
- https://github.com/ActiveState/appdirs + https://github.com/platformdirs/platformdirs: `~/.local/share/<AppName>`
- https://github.com/pypa/virtualenv
- https://github.com/pypa/pipenv/blob/v2023.11.15/pipenv/utils/shell.py#L183: `os.environ.get("XDG_DATA_HOME", "~/.local/share"), "virtualenvs"`
- https://bun.sh/docs/api/utils#bun-resolvesync
- https://nodejs.org/api/os.html#oshomedir
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for-await...of
- https://bun.sh/docs/api/file-io
- https://nodejs.org/api/fs.html#file-access-constants
- https://www.basedash.com/blog/javascript-check-if-file-exists
- https://www.builder.io/blog/bun-vs-node-js
- https://bun.sh/docs/runtime/nodejs-apis
- https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account: `tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy`
- https://github.com/oven-sh/bun/issues/8403
- https://github.com/oven-sh/bun/issues/8572
- https://docs.conda.io/projects/conda/en/stable/user-guide/tasks/manage-environments.html#viewing-a-list-of-your-environments
- https://docs.conda.io/projects/conda/en/stable/user-guide/tasks/manage-environments.html#removing-an-environment
- https://docs.conda.io/projects/conda/en/latest/commands/clean.html
- https://stackoverflow.com/questions/24902061/is-there-a-repr-equivalent-for-javascript: `JSON.stringify()`

## Commands

```bash
npm install -D bun
```

```bash
rm -rf ~/.bun
```

```bash
bun init
```

```bash
bun add --dev @types/bun
```

```bash
bun --help
```

```bash
rm -rf node_modules/ && bun install
```

```bash
du -hs ~/.local/share/virtualenvs/
```

```bash
du -hs ~/miniconda3/envs/
```

```bash
du -hs ~/miniconda3/
```

```bash
conda env list --json
```

```bash
conda env list --help
```

```bash
conda info --base
```

```bash
conda info --base --json
```

```bash
conda clean --all
```
